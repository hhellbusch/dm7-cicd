#!/usr/bin/env bash

set -x

KIE_USER=adminUser
KIE_PASS=test123!

source jenkins/build-vars

env | grep POM_VERSION
env | grep POM_NAME
env | grep POM_GROUPID

CONTAINER=foo

KIE_HOST=dm7-hooks-1
KIE_PORT=8080

# look at the docs at http://w10-ds-1:8080/kie-server/docs/ 
# to determine if there's a "better way"...

curl --user "$KIE_USER:$KIE_PASS" -X DELETE http://${KIE_HOST}:${KIE_PORT}/kie-server/services/rest/server/containers/${CONTAINER}

curl --user "$KIE_USER:$KIE_PASS" \
  -H "Content-Type: application/json" \
  -X PUT -d "{\"container-id\" : \"${CONTAINER}\",\"release-id\" : {\"group-id\" : \"${POM_GROUPID}\",\"artifact-id\" : \"${POM_NAME}\",\"version\" : \"${POM_VERSION}\"}}" \
  http://${KIE_HOST}:${KIE_PORT}/kie-server/services/rest/server/containers/${CONTAINER}