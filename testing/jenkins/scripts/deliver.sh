#!/usr/bin/env bash

set -x

source jenkins/build-vars

env | grep POM_VERSION
env | grep POM_NAME
env | grep POM_GROUPID

mvn deploy:deploy-file \
  -Dmaven.install.skip=true \
  -DgroupId=$POM_GROUPID \
  -DartifactId=$POM_NAME \
  -Dversion=$POM_VERSION \
  -Dpackaging=jar \
  -Dfile=target/$POM_NAME-$POM_VERSION.jar \
  -DgeneratePom=true \
  -DupdateReleaseInfo=true \
  -Durl="http://admin:admin123@w10-nexus:8081/repository/workspace"